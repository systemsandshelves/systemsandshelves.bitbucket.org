$(document).ready(function() {
    var $first = $('#menu-1'),
        $line = $('#line-menu');
    $line.css({
        'width': $first.width(),
        'left': $first.offset().left
    });
    $('#menu > div').mouseenter(function() {
        var $this = $(this),
            width = $this.width(),
            left = $this.offset().left;
        $line.stop().animate({
            'left': left,
            'width': width
        }, "slow");
    });
});